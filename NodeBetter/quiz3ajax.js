const express = require('express');
const fs = require('fs');
const app = express();

const port = 3000;
const jsonDataFile = 'trips.json';

var messagesArray = [];

function saveAllData() {
    var dataStr = JSON.stringify(messagesArray, null, " ");
    fs.writeFileSync(jsonDataFile, dataStr);
}

function loadAllData() {
    if (fs.existsSync(jsonDataFile)) {
        var dataStr = fs.readFileSync(jsonDataFile);
        messagesArray = JSON.parse(dataStr);
    }
}

loadAllData();


app.get('/add', (request, response) => {
    var id = request.query['id']-0;
    var passport = request.query['passport'];
    var airpcode = request.query['airpcode'];
    var city = request.query['city'];
    var depdate = request.query['depdate'];
    var retdate = request.query['retdate'];
    
 if (id == undefined || passport == '' || airpcode == '' || city == '' || depdate == '' || retdate == '') {
        response.send("Error: invalid data sent");
        return;
    }
     
    var line = { id:id, passport: passport, airpcode: airpcode, city: city, depdate:depdate, retdate:retdate};
    messagesArray.push(line);
    saveAllData();
    response.send(`Item ${id} added`);
});

app.get('/getlist', (request, response) => {//从后往前显示记录
    var html = "";
    for (var i = 0; i < messagesArray.length; i++) {
        var f = messagesArray[i];
        html += `<option value="${f.id}">${f.id}: ${f.passport} travels to ${f.city}(${f.airpcode}) on ${f.depdate} and return on ${f.retdate}</option>`;
    }
    response.send(html);
});
app.get('/delete',(request,response)=>{
    var id = request.query['id']-0;
  for(var i =0;i<messagesArray.length;i++){
       if(messagesArray[i].id == id){
           messagesArray.splice(i,1);
           saveAllData();
           response.send(`Item ${id} deleted`);
           return;
       }
   }
   response.send(`error deleting friend, id=${id} not found`);
});
app.get('/getlistastable', (request, response) => {
    var tripscount = {};
    var html = "<table><tbody id='statistic'>";
    for (var i = 0; i < messagesArray.length; i++) {
        if (tripscount[messagesArray[i].airpcode] == undefined) {
            tripscount[messagesArray[i].airpcode] = 1;
        } else {
            tripscount[messagesArray[i].airpcode]++;
        }
    }
    //console.log(tripscount);
    Object.keys(tripscount).forEach(function (key) {
        html += "<tr><td class='code'>"+key+"</td><td class='totalnumber'>"+tripscount[key]+"</td></tr>";
    });
    
    html += "</tbody></table>";
    response.send(html);
});
app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }

    console.log(`server is listening on ${port}`);
});
