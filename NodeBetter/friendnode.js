const express = require('express');
const app = express();
const fs = require('fs');
const port = 3000;
const jsonDataFile = 'data.json';

friendArray = [];
function saveAllData() {
    var dataStr = JSON.stringify(friendArray, null, " ");//为了将数组转化为string并且显示分行
    fs.writeFileSync(jsonDataFile, dataStr);//将string数组写入jsonDataFile
}
function loadAllData() {
    if (fs.existsSync(jsonDataFile)) {//这个函数为了以同步的方法检测目录是否存在。如果目录存在 返回 true ，如果目录不存在 返回falsefs.existsSync(pach);
//由于该方法属于fs模块，使用前需要引入fs模块 var fs = require('fs');接收参数：（path:将检测的目录路径）
        var dataStr = fs.readFileSync(jsonDataFile);
        friendArray = JSON.parse(dataStr);
    }
}
loadAllData();

app.get('/add', (request, response) => {
    var id = request.query['id']-0;
    var name = request.query['name'];
    var age = request.query['age']-0;
     if(id<1  || name.length <1 ||age<1){
         response.send(`Error`);
         return;
     }
    var friend = {id:id,name:name,age:age};
    friendArray.push(friend);
    saveAllData();
    response.send(`Person ${id} added`);
});

app.get('/delete',(request,response)=>{
    var id = request.query['id']-0;
  for(var i =0;i<friendArray.length;i++){
       if(friendArray[i].id == id){
           friendArray.splice(i,1);
           saveAllData();
           response.send(`Person ${id} deleted`);
           return;
       }
   }
   response.send(`error deleting friend, id=${id} not found`);
});

app.get('/fetchall',(request,response)=>{
     var html ="";
     for(var i =0;i<friendArray.length;i++){
         var f =friendArray[i];
         html += `<option value="${f.id}">${f.id}: ${f.name} is ${f.age} y/o</option>`;
     }
     response.send(html);
});
app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }

    console.log(`server is listening on ${port}`);
});


