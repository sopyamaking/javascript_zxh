const express = require('express');
const fs = require('fs');
const app = express();

const port = 3005;
const jsonDataFile = 'chatlines.json';


var messagesArray = [];

function saveAllData() {
    var dataStr = JSON.stringify(messagesArray, null, " ");
    fs.writeFileSync(jsonDataFile, dataStr);
}

function loadAllData() {
    if (fs.existsSync(jsonDataFile)) {
        var dataStr = fs.readFileSync(jsonDataFile);
        messagesArray = JSON.parse(dataStr);
    }
}

loadAllData();

app.get('/send', (request, response) => {
    var name = request.query['name'];
    var color = request.query['color'];
    var msg = request.query['msg'];
    console.log(`/send name=${name}, color=${color}, msg=${msg}`);
    // TODO: verify all 3 parameters look valid
    if (name == undefined || name == '' || color == '' || msg == '') {
        response.send("Error: invalid data sent");
        return;
    }
    var chatLine = {name: name, color: color, msg: msg};
    messagesArray.push(chatLine);
    saveAllData();
    response.send(`Message sent!`);
});

app.get('/update', (request, response) => {//从后往前显示记录
    var html = "";
    for (var i = 0; i < messagesArray.length; i++) {
        var f = messagesArray[i];
        if (i == messagesArray.length - 1)
            html += `<option style="color:${f.color}" selected>${f.name}: ${f.msg}</option>`;//这是 将光标固定在最后一个。这样自动就锁定最后的记录
        else
            html += `<option style="color:${f.color}">${f.name}: ${f.msg}</option>`;
    }
    response.send(html);
});

app.get('/statistics', (request, response) => {
    var html = "";
    var msgs = {}, chats = {}; //
    //var linesArray = [], chatsArray = [];

    for (var i = 0; i < messagesArray.length; i++) {
        //console.log(hash[messagesArray[i].name]);
        if (msgs[messagesArray[i].name] == undefined) {//msg数组里面是键值对数组，人名（key）：出现次数（value），如果没出现过，赋值一次
            msgs[messagesArray[i].name] = 1;
            chats[messagesArray[i].name] = messagesArray[i].msg.length;
        } else {
            msgs[messagesArray[i].name]++;//出现多次就自加
            chats[messagesArray[i].name] += messagesArray[i].msg.length;
        }
    }
    console.log("lines=" + msgs + ",chats=" + chats);
    Object.keys(msgs).forEach(function (key) {//对于msgs数组里的每个key人名，操作一次function
        console.log(key + ":" + msgs[key]);
        html += "<tr><td class='name'>"+key+"</td><td class='totalmsgs'>"+msgs[key]+"</td><td class='totalchars'>"+chats[key]+"</td></tr>";
        console.log(key + ":" + chats[key]);
    });   
    response.send(html);
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }
    console.log(`server is listening on ${port}`);
});

//function dynamicSort(property) {//从大到小排列
//    return function (a, b) {
//        //var result= (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
//        return b[property] - a[property];
//    }
//}


